// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore"; 
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
export const firebaseConfig = {
  apiKey: 'AIzaSyDRmHQ_27xR_Udjmqg3S02S8C5Nma2n1tI',
  authDomain: 'tg-shop-53386.firebaseapp.com',
  projectId: 'tg-shop-53386',
  storageBucket: 'tg-shop-53386.appspot.com',
  messagingSenderId: '207510330543',
  appId: '1:207510330543:web:dcf2e248d4ae06bd23485a'
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
